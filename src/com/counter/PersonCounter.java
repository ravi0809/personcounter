package com.counter;

public class PersonCounter {

	private static final PersonCounter _instance=new PersonCounter();
	
	private int counter=0;
	
	private PersonCounter() {
	}
	
	public static PersonCounter getInstance() {
		return _instance;
	}
	
	public void incrementCount() {
		++_instance.counter;
	}
	
	public int getPersonCount() {
		return _instance.counter;
	}
	
	public void decrementCount() {
		if(counter >0) {
			--_instance.counter;
		}
	}
}

