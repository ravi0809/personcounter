package com.controller;

import com.counter.PersonCounter;
import com.pi4j.io.gpio.*;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by ravi on 27/7/17.
 */
@WebServlet(name = "ListenGpio")
public class ListenGpio extends HttpServlet {
    GpioPinDigitalInput pin3=null;
    GpioPinDigitalInput pin4=null;
    GpioController gpio= null;
    PersonCounter pc=PersonCounter.getInstance();

    @Override
    public void init() throws ServletException{
        super.init();
        initGpioConfig();
        System.out.println("\n\n\n     listenGpio init called");

    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private void listen(){

    }
    private void initGpioConfig(){
    	
        gpio= GpioFactory.getInstance();
        pin3=gpio.provisionDigitalInputPin(RaspiPin.GPIO_03, PinPullResistance.PULL_UP);
        pin3.setShutdownOptions(true);
        pin3.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
            	try {
					if(PinState.HIGH.equals(event.getState())){
						pc.incrementCount();
						System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState()+"   "+pc.getPersonCount());
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

            }
        });
        
        pin4=gpio.provisionDigitalInputPin(RaspiPin.GPIO_04, PinPullResistance.PULL_UP);
        pin4.setShutdownOptions(true);
        pin4.addListener(new GpioPinListenerDigital() {
            @Override
            public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event) {
            	try {
					if(PinState.HIGH.equals(event.getState())){
						pc.decrementCount();
						System.out.println(" --> GPIO PIN STATE CHANGE: " + event.getPin() + " = " + event.getState()+"   "+pc.getPersonCount());
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });
    }
}
