<%@page import="com.counter.PersonCounter"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/jquery-ui.css"/>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>
Control your Appliances
</h1>
<%!PersonCounter pc=PersonCounter.getInstance(); %>

<form name="ajaxForm" method="post">
    <table>
        <tr>
            <td>
                Persons in the Room:
            </td>
            <td>
                <%pc.getPersonCount(); %>
            </td>
        </tr>

    </table>

</form>


</form>
<script>
function toggleButton(){
    var buttonState="off";
    if($("#bulb").prop("checked")){
        buttonState="on";
    }
    $.ajax({
       url:"controllerServlet?fromAjax=yes&buttonState="+buttonState
    });
}
</script>
</body>
</html>
